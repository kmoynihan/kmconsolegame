package com.kristian;

import com.kristian.model.GameBoard;
import com.kristian.model.Player;
import com.kristian.model.Referee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Kristian on 27/02/16.
 * Called after the application is full bootstrapped and all beans are injected into the Spring context.
 */
@Component
public class GameLoader implements CommandLineRunner
{

    private static final Logger LOGGER = Logger.getLogger(GameLoader.class.getName());

    @Autowired
    private GameBoard gameBoard;

    @Autowired
    private Referee referee;


    /**
     * Adds 10 players to the game and sets up the GameBaord object & starts the game.
     *
     * @param strings
     * @throws Exception
     */
    @Override
    public void run(String... strings) throws Exception
    {
        LOGGER.log(Level.INFO, "Launching Game.");

        for (int i = 0; i < GameBoard.NUM_PLAYERS; i++)
        {
            Player player = new Player("Player_" + i, gameBoard, referee);
            gameBoard.addPlayer(player);
        }

        gameBoard.initGameLoop();
        gameBoard.setReferee(referee);
        gameBoard.start();
    }
}
