package com.kristian.config;

import com.kristian.model.GameBoard;
import com.kristian.model.Referee;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Creates the bean objects needed for the game.
 * Created by Kristian on 27/02/16.
 */
@Configuration
public class GameSetup
{
    /**
     * Creates the GameBoard Spring Bean
     *
     * @return the game board
     */
    @Bean
    public GameBoard gameBoard()
    {
        return new GameBoard();
    }

    /**
     * Creates the Referee Spring Bean
     *
     * @return the referee
     */
    @Bean
    public Referee referee()
    {
        return new Referee(gameBoard());
    }

}
