package com.kristian;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


/**
 * Created by Kristian on 27/02/16.
 */
@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan
public class App
{
    /**
     * The entry point of application.
     * Bootstraps the Spring Context
     *
     * @param args the input arguments
     */
    public static void main(String[] args)
    {
        SpringApplication.run(App.class, args);
    }
}


