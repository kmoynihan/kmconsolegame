package com.kristian.timers;


import com.kristian.model.Player;
import com.kristian.model.Referee;

import java.util.TimerTask;

/**
 * Simple timer task used to control duration of time a player is not allowed to beginAskingToMoveInRandomDirection
 * after receiving a yellow card for committing a foul
 * Created by Kristian on 27/02/16.
 */
public class CoolDownTimer extends TimerTask
{
    private Referee referee = null;
    private Player player = null;

    /**
     * Instantiates a new Cool down timer.
     *
     * @param player  the player
     * @param referee the referee
     */
    public CoolDownTimer(Player player, Referee referee)
    {
        this.player = player;
        this.referee = referee;
    }

    /**
     * After 10 seconds, the referee is asked if the player can return.
     */
    @Override
    public void run()
    {
        referee.allowPlayerReturn(player);
    }
}
