package com.kristian.timers;

import com.kristian.model.Direction;
import com.kristian.model.GameBoard;
import com.kristian.model.Player;

import java.util.TimerTask;

/**
 * Simple timer task used to control the frequency with which a player requests to move
 * along the game board.
 * Created by Kristian on 27/02/16.
 */
public class MoveTimer extends TimerTask
{

    private GameBoard gameBoard;
    private Direction direction;
    private Player player;

    /**
     * Instantiates a new Move timer.
     *
     * @param direction the direction
     * @param player    the player
     * @param gameboard the gameboard
     */
    public MoveTimer(Direction direction, Player player, GameBoard gameboard)
    {
        this.direction = direction;
        this.player = player;
        this.gameBoard = gameboard;
    }

    /**
     * Each second, the gameboard is called to get the referee to allow player to move.
     */
    @Override
    public void run()
    {
        gameBoard.requestToMove(player, direction);
    }
}
