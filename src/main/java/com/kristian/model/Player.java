package com.kristian.model;

import com.kristian.timers.CoolDownTimer;
import com.kristian.timers.MoveTimer;

import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class defines the player and the timers used to control the gameplay
 * <p>
 * On making the first illegal move, the player is sent off for 10 seconds after which
 * they can ask to rejoin the game.
 * <p>
 * If a player is given 2 yellow cards by the referee, they are permanently removed from the game.
 * <p>
 * Created by Kristian on 27/02/16.
 */
public class Player
{

    private static final Logger LOGGER = Logger.getLogger(Player.class.getName());
    /**
     * The Current state.
     */
    public PlayerState currentState;
    private Referee referee;
    private Timer fouledCooldownTimer = new Timer();
    private Timer moveTimer = new Timer();
    private String name;
    private GameBoard gameboard;
    private int yellowCardCount = 0;
    private GridLocation gridLocation;

    /**
     * Instantiates a new Player.
     *
     * @param name      the name
     * @param gameboard the gameboard
     * @param referee   the referee
     */
    public Player(String name, GameBoard gameboard, Referee referee)
    {
        this.name = name;
        this.gameboard = gameboard;
        this.referee = referee;
    }

    /**
     * Gets player name.
     *
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Gets current state.
     *
     * @return the current state
     */
    public PlayerState getCurrentState()
    {
        return currentState;
    }

    /**
     * Sets current state.
     *
     * @param currentState the current state
     */
    public void setCurrentState(PlayerState currentState)
    {
        this.currentState = currentState;
    }


    /**
     * Gets yellow card count.
     *
     * @return the yellow card count
     */
    public int getYellowCardCount()
    {
        return yellowCardCount;
    }


    /**
     * Called by the GameBoard when a player has Committed foul.
     * Increments the yellow card count and starts the cooldown timer
     */
    public void committedFoul()
    {
        yellowCardCount++;
        if (yellowCardCount < 2)
        {
            fouledCooldownTimer.schedule(new CoolDownTimer(this, referee), GameBoard.PLAYER_FOUL_DURATION);
        }
    }

    /**
     * Returns true if the player has accumulated 2 yellow cards.
     * If so, they cannot rejoin the game
     *
     * @return the boolean
     */
    public boolean shouldBanPlayer()
    {
        return yellowCardCount == 2;
    }


    /**
     * Cancels timers.
     */
    public void cancelTimers()
    {
        fouledCooldownTimer.cancel();
        moveTimer.cancel();
    }

    /**
     * Gets grid location.
     *
     * @return the grid location
     */
    public GridLocation getGridLocation()
    {
        return gridLocation;
    }

    /**
     * Sets grid location of the player.
     *
     * @param gridLocation the grid location
     */
    public void setGridLocation(GridLocation gridLocation)
    {
        this.gridLocation = gridLocation;
    }

    /**
     * Referee has allowed the move request and the player can not move.
     */
    public void moveAccepted()
    {
        beginAskingToMoveInRandomDirection();
    }

    /**
     * Starts the player requesting the Game Board to move every second.
     */
    public void beginAskingToMoveInRandomDirection()
    {
        Direction direction = Direction.randomDirection();
        LOGGER.log(Level.INFO, String.format("%s is located at Row: %d Column: %d and wants to move: %s", getName(), getGridLocation().getRow(),
                getGridLocation().getColumn(), direction));
        moveTimer.schedule(new MoveTimer(direction, this, gameboard), GameBoard.MOVEMENT_TICK);
    }
}
