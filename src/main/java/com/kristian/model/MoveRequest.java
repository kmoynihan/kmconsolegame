package com.kristian.model;

/**
 * Created by Kristian on 27/02/16.
 */
public class MoveRequest
{
    private Direction direction = null;
    private Player player = null;

    /**
     * Instantiates a new Move request.
     *
     * @param player    the player
     * @param direction the direction
     */
    public MoveRequest(Player player, Direction direction) {
        this.player = player;
        this.direction = direction;
    }

    /**
     * Gets direction.
     *
     * @return the direction
     */
    public Direction getDirection() {
        return direction;
    }

    /**
     * Gets player.
     *
     * @return the player
     */
    public Player getPlayer() {
        return player;
    }
}
