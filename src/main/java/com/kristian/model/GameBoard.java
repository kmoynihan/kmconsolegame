package com.kristian.model;

import org.apache.commons.lang.math.RandomUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class captures the main logic of the Application, controlling the interaction of the players and the referee.
 * <p>
 * Created by Kristian on 26/02/16.
 */
public class GameBoard implements Runnable
{

    /**
     * 1 Second Duration
     * The constant MOVEMENT_TICK.
     */
    public static final long MOVEMENT_TICK = 1000L;
    /**
     * 10 Seconds Duration
     * The constant PLAYER_FOUL_DURATION.
     */
    public static final long PLAYER_FOUL_DURATION = 10000L;
    /**
     * The constant NUM_PLAYERS.
     */
    public static final int NUM_PLAYERS = 10;
    private static final Logger LOGGER = Logger.getLogger(GameBoard.class.getName());
    private static int BOARD_SIZE = 100;
    private int bannedPlayerCount;
    private int playersWithYellowCardsCount;
    private Referee referee;
    private Set<Player> players = new HashSet<>();
    private BlockingQueue<MoveRequest> movementQueue;
    private GridLocation[][] gridLocations;


    /**
     * Instantiates a new Game board.
     * Sets up the movement queue and board grid.
     */
    public GameBoard()
    {
        movementQueue = new ArrayBlockingQueue<>(100, true);
        gridLocations = new GridLocation[BOARD_SIZE][BOARD_SIZE];
        bannedPlayerCount = 0;
        playersWithYellowCardsCount = 0;
        initialiseGrid();
    }


    /**
     * Main run loop of the game.
     * Takes movement requests from the queue and process them through the referee.
     * Handles the win condition when 1 player is remaining and shuts down the game.
     *
     * If a player is on a cool down timer and  has not yet been added back to the current players queue before next
     * loop iteration, they will counted as out of the game and the remaining player will win.
     */
    @Override
    public void run()
    {
        LOGGER.log(Level.INFO, "Running Game Loop");

        while (true)
        {
            try
            {
                MoveRequest moveRequest = movementQueue.take();
                Player player = moveRequest.getPlayer();
                Direction direction = moveRequest.getDirection();

                // Player could have been sent off at this point so must be added back in to the game.
                if (!players.contains(player) && player.getCurrentState() != PlayerState.BANNED)
                {
                    player.setCurrentState(PlayerState.PLAYING);
                    players.add(player);
                }

                LOGGER.log(Level.INFO, String.format("Handling a move request for %s in direction: %s", player.getName(), direction));

                if (referee.moveIsPossible(player, direction))
                {
                    LOGGER.log(Level.INFO, String.format("%s move is possible. Allowing player to continue moving in current direction.", player.getName()));

                    movePlayer(player, direction);
                }
                else
                {
                    LOGGER.info(String.format("%s's move is illegal. Foul committed..", player.getName()));

                    player.committedFoul();
                    referee.yellowFlagPlayer(player);
                }

                LOGGER.log(Level.INFO, "");
                LOGGER.log(Level.INFO, "Players currently playing: {0}.", players.size());
                LOGGER.log(Level.INFO, "Players currently banned: {0}.", bannedPlayerCount);
                LOGGER.log(Level.INFO, "Players with yellow cards: {0}.", playersWithYellowCardsCount);
                LOGGER.log(Level.INFO, "");

                if (players.size() == 1 && bannedPlayerCount == 9)
                {
                    cleanupAndShutdown();
                    break;
                }

            }
            catch (InterruptedException ex)
            {
                LOGGER.log(Level.SEVERE, "Could not process requests.", ex);
                throw new IllegalStateException("Could not process requests.", ex);
            }
        }
    }

    /**
     * Removes the player from the game.
     *
     * @param player the player
     */
    public void sendPlayerOff(Player player)
    {

        players.remove(player);
        player.getGridLocation().setPlayer(null);

        if(player.shouldBanPlayer())
        {
            LOGGER.log(Level.INFO, String.format("%s has been banned", player.getName()));
            player.setCurrentState(PlayerState.BANNED);
            bannedPlayerCount++;
        }
        else
        {
            LOGGER.log(Level.INFO, String.format("%s has been removed from play for 10 seconds.", player.getName()));
            player.setCurrentState(PlayerState.WAITING);
            playersWithYellowCardsCount++;
        }
    }


    /**
     * Called as the final action of the game. Clears out the queue, cancels any remaining players move requests & timers and outputs the winner
     */
    private void cleanupAndShutdown()
    {
        ArrayList<MoveRequest> processes = new ArrayList<>();
        movementQueue.drainTo(processes);

        for (MoveRequest moveRequest : processes)
        {
            Player player = moveRequest.getPlayer();
            player.cancelTimers();
        }

        Player winningPlayer = players.iterator().next();
        LOGGER.log(Level.INFO, "");
        LOGGER.log(Level.INFO, String.format("#####WINNER########  - {%s} WINS!", winningPlayer.getName()));
        LOGGER.log(Level.INFO, "");
        LOGGER.log(Level.INFO, "Game Over Man, Game Over.");
        LOGGER.log(Level.INFO, "");
        LOGGER.log(Level.INFO, "");

        System.exit(0);
    }


    /**
     * Gets grid location.
     *
     * @param row    the row
     * @param column the column
     * @return the grid location
     */
    public final GridLocation getGridLocation(int row, int column)
    {
        try
        {
            return gridLocations[row][column];
        }
        catch (ArrayIndexOutOfBoundsException ex)
        {
            LOGGER.log(Level.SEVERE, String.format("Invalid Grid Location Row: %d Column %d requested", row, column));
            return null;
        }
    }

    /**
     * Adds a move request for the player to the movement queue
     *
     * @param player    the player
     * @param direction the direction
     */
    public void requestToMove(Player player, Direction direction)
    {
        if (players.contains(player))
        {
            try
            {
                movementQueue.put(new MoveRequest(player, direction));
            }
            catch (InterruptedException ex)
            {
                LOGGER.log(Level.SEVERE, "Could not add new Move Request to the Movement Queue", ex.getMessage());
            }
        }
    }

    /**
     * Adds player to the grid.
     * Skips occupied Grid Locations so each player gets a unique starting position
     *
     * @param player the player
     */
    public void addPlayer(Player player)
    {
        int column = RandomUtils.nextInt(BOARD_SIZE);
        int row = RandomUtils.nextInt(BOARD_SIZE);

        GridLocation location = gridLocations[row][column];

        if (location.isOccupied())
        {
            addPlayer(player);
        }

        location.setPlayer(player);
        player.setGridLocation(location);

        players.add(player);
    }

    /**
     * Sets referee.
     *
     * @param referee the referee
     */
    public void setReferee(Referee referee)
    {
        this.referee = referee;
    }

    /**
     * Initializing the board grid & start the main thread.
     */
    public void initGameLoop()
    {

        Thread t = new Thread(this);
        t.start();

        LOGGER.log(Level.INFO, "Game Started");
    }

    private void initialiseGrid()
    {
        for (int row = 0; row < BOARD_SIZE; row++)
        {
            for (int column = 0; column < BOARD_SIZE; column++)
            {
                GridLocation gridLocation = new GridLocation(column, row, this);
                gridLocations[row][column] = gridLocation;
            }
        }
    }



    /**
     * Begin the game by making each player move.
     */
    public void start()
    {
        LOGGER.log(Level.INFO, "Initialising players.");
        players.forEach(Player::beginAskingToMoveInRandomDirection);
    }


    /**
     * Handles moving the player from one location to the next in the direction they are travelling.
     *
     * @param player
     * @param direction
     */
    private void movePlayer(Player player, Direction direction)
    {
        GridLocation newGridLocation = referee.getSafeDirection(player.getGridLocation(), direction);
        newGridLocation.setPlayer(player);

        player.getGridLocation().setPlayer(null);
        player.setGridLocation(newGridLocation);
        player.moveAccepted();
    }
}
