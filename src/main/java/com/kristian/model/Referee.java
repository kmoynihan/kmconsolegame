package com.kristian.model;

import java.util.logging.Logger;

/**
 * This class controls the rules of the game by checking the players location
 * to allow a move or punish a player for violations.
 * Created by Kristian on 26/02/16.
 */
public class Referee
{

    private static final Logger LOGGER = Logger.getLogger(Referee.class.getName());

    private GameBoard gameBoard;

    /**
     * Instantiates a new Referee.
     *
     * @param gameBoard the game board
     */
    public Referee(GameBoard gameBoard)
    {
        this.gameBoard = gameBoard;
    }

    /**
     * Called after a player has completed their 10 second cool down from being sent off.
     *
     * @param player the player
     */
    public void allowPlayerReturn(Player player)
    {
        LOGGER.info("Referee has allowed player to return the game.");

        gameBoard.addPlayer(player);
        player.beginAskingToMoveInRandomDirection();
    }

    /**
     * Yellow flag player.
     *
     * @param player the player
     */
    public void yellowFlagPlayer(Player player)
    {
        LOGGER.info(String.format("Referee has given %s a yellow card. Card count: %d", player.getName(), player.getYellowCardCount()));
        gameBoard.sendPlayerOff(player);
    }

    /**
     * Checks if the move is possible in current direction of travel
     * If there is another player in the next GridLocation, or within 1 metre in any adjacent grid locations, the move is not allowed
     * & the referee will give the player a yellow card
     *
     * @param player    the player
     * @param direction the direction
     * @return boolean if the move is permitted
     */
    public boolean moveIsPossible(Player player, Direction direction)
    {
        boolean isPossibleMove = false;

        GridLocation gridLocationInDirection = getSafeDirection(player.getGridLocation(), direction);

        LOGGER.info(String.format("Checking if the move for %s to Column: %d Row: %d is permitted.", player.getName(), gridLocationInDirection
                .getColumn(), gridLocationInDirection.getRow()));

        if (!gridLocationInDirection.isOccupied() && (!adjacentGridLocationsOccupied(gridLocationInDirection, player)))
        {
            isPossibleMove = true;
        }

        return isPossibleMove;
    }

    /**
     * Simply turns the player around if they are about to hit the edge of the grid.
     * @param currentGridLocation
     * @param direction
     * @return
     */
    public GridLocation getSafeDirection(GridLocation currentGridLocation, Direction direction)
    {
        int row = currentGridLocation.getRow();
        int column = currentGridLocation.getColumn();

        if (row <= 0 && direction == Direction.DOWN)
        {
            LOGGER.info("Player has hit bottom edge, reversing direction");
            return currentGridLocation.getGridLocationInDirection(Direction.UP);
        }
        if  (row >= 99 && direction == Direction.UP)
        {
            LOGGER.info("Player has hit top edge, reversing direction");
            return currentGridLocation.getGridLocationInDirection(Direction.DOWN);
        }

        if (column <= 0 && direction == Direction.LEFT)
        {
            LOGGER.info("Player has hit left edge, reversing direction");
            return currentGridLocation.getGridLocationInDirection(Direction.RIGHT);
        }
        if  (column >= 99 && direction == Direction.RIGHT)
        {
            LOGGER.info("Player has hit right edge, reversing direction");
            return currentGridLocation.getGridLocationInDirection(Direction.LEFT);
        }

        return currentGridLocation.getGridLocationInDirection(direction);
    }


    /**
     * Checks 2 grids locations in all directions to see if there is another player in occupation
     * @param gridLocation
     * @param player
     * @returns true if adjacent grid location has a player
     */
    private boolean adjacentGridLocationsOccupied(GridLocation gridLocation, Player player)
    {
        return isAdjacentGridLocationOccupied(gridLocation.getLeftGridLocation(), player)
                || isAdjacentGridLocationOccupied(gridLocation.getTwoGridsLeftGridLocation(), player)

                || isAdjacentGridLocationOccupied(gridLocation.getRightGridLocation(), player)
                || isAdjacentGridLocationOccupied(gridLocation.getTwoGridsRightGridLocation(), player)

                || isAdjacentGridLocationOccupied(gridLocation.getTopGridLocation(), player)
                || isAdjacentGridLocationOccupied(gridLocation.getTwoTopGridsLocation(), player)

                || isAdjacentGridLocationOccupied(gridLocation.getBottomGridLocation(), player)
                || isAdjacentGridLocationOccupied(gridLocation.getTwoGridsBottomGridLocation(), player);
    }

    private boolean isAdjacentGridLocationOccupied(GridLocation gridLocation, Player player)
    {
        if (gridLocation != null)
        {
            boolean isOccupied = gridLocation.isOccupied();

            if (isOccupied)
            {
                //As each adjacent location of the desired grid location will be checked, we are adjacent to the one we
                //want to move to, so this must be excluded.
                if (gridLocation.getPlayer().getName().equals(player.getName()))
                {
                    return false;
                }
            }

            return isOccupied;
        }
        return false;
    }

}
