package com.kristian.model;

import java.util.logging.Logger;

/**
 * Contains the row and column locations within the game grid.
 * Created by Kristian on 27/02/16.
 */
public class GridLocation
{

    private static final Logger LOGGER = Logger.getLogger(GridLocation.class.getName());
    private int column;
    private int row;
    private Player player;
    private GameBoard gameBoard;

    /**
     * Instantiates a new Grid location.
     *
     * @param column    the column
     * @param row       the row
     * @param gameBoard the game board
     */
    public GridLocation(int column, int row, GameBoard gameBoard)
    {
        this.column = column;
        this.row = row;
        this.gameBoard = gameBoard;
    }

    /**
     * Gets player.
     *
     * @return the player
     */
    public Player getPlayer()
    {
        return player;
    }

    /**
     * Sets player.
     *
     * @param player the player
     */
    public void setPlayer(Player player)
    {
        this.player = player;
    }

    /**
     * Gets column.
     *
     * @return the column
     */
    public int getColumn()
    {
        return column;
    }

    /**
     * Gets row.
     *
     * @return the row
     */
    public int getRow()
    {
        return row;
    }


    /**
     * Is occupied boolean.
     *
     * @return the boolean
     */
    public boolean isOccupied()
    {
        return player != null;
    }

    /**
     * Gets grid location in direction.
     *
     * @param direction the direction
     * @return the grid location in direction
     */
    public GridLocation getGridLocationInDirection(Direction direction)
    {
        switch (direction)
        {
            case LEFT:
                return getLeftGridLocation();
            case RIGHT:
                return getRightGridLocation();
            case UP:
                return getTopGridLocation();
            case DOWN:
                return getBottomGridLocation();
            default:
                return null;
        }
    }


    /**
     * Gets left grid location.
     *
     * @return the left grid location
     */
    public GridLocation getLeftGridLocation()
    {
        return gameBoard.getGridLocation(row, column - 1);
    }

    /**
     * Gets grid location two to the left grid location.
     *
     * @return the left grid location
     */
    public GridLocation getTwoGridsLeftGridLocation()
    {
        return gameBoard.getGridLocation(row, column - 2);
    }

    /**
     * Gets top grid location.
     *
     * @return the top grid location
     */
    public GridLocation getTopGridLocation()
    {
        return gameBoard.getGridLocation(row + 1, column);
    }


    /**
     * Gets grid location two to the top grid location.
     *
     * @return the top grid location
     */
    public GridLocation getTwoTopGridsLocation()
    {
        return gameBoard.getGridLocation(row + 2, column);
    }

    /**
     * Gets right grid location.
     *
     * @return the right grid location
     */
    public GridLocation getRightGridLocation()
    {
        return gameBoard.getGridLocation(row, column + 1);
    }


    /**
     * Gets grid location two to the right grid location.
     *
     * @return the right grid location
     */
    public GridLocation getTwoGridsRightGridLocation()
    {
        return gameBoard.getGridLocation(row, column + 2);
    }

    /**
     * Gets bottom grid location.
     *
     * @return the bottom grid location
     */
    public GridLocation getBottomGridLocation()
    {
        return gameBoard.getGridLocation(row - 1, column);
    }


    /**
     * Gets grid location two to the bottom grid location.
     *
     * @return the bottom grid location
     */
    public GridLocation getTwoGridsBottomGridLocation()
    {
        return gameBoard.getGridLocation(row - 2, column);
    }
}
