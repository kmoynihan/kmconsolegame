package com.kristian.model;

import java.util.Random;

/**
 * ENUM containing the directions a player can move and a method to return a random direction
 * Created by Kristian on 27/02/16.
 */
public enum Direction
{

    UP, DOWN, LEFT, RIGHT;

    /**
     * Randoms direction direction.
     * Used when player starts to first move.
     *
     * @return the direction
     */
    public static Direction randomDirection()
    {
        Direction[] directions = Direction.values();
        Random generator = new Random();
        return directions[generator.nextInt(directions.length)];
    }
}
