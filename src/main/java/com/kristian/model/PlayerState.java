package com.kristian.model;

/**
 * Captures the current state the player is in.
 * PLAYING - Actively playing
 * WAITING - Received a yellow card and waiting to return
 * BANNED - Received two yellow cards and cannot return.
 * Created by Kristian on 29/02/16.
 */
public enum PlayerState
{
    PLAYING, WAITING, BANNED
}
