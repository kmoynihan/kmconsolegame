Game Exercise
=================================================================================
A playing area is 100m x 100m. A game has a referee and 10 players. A player moves 1m every second and starts out in a random place on the playing area. A referee has to give a yellow card to a player if that player moves within 2m of another player. It is the player that moves to within 2m gets the card. If a player gets 2 yellow cards, the player is ejected from the game for 10 seconds. When a player is off 10 secs the player needs to ask the referee if they are eligible to play again. The referee will let the player return to playing the first time this happens, but not subsequent times. The last player left playing is the winner.

Write a console application that emulates this activity.


Assumptions
=================================================================================
1. 100m x 100m Game area is represented by a 2 dimensional array of 100*100 'Grid Locations'. Each one of these is represented by an Object.
2. Each location on the 'GameBoard' can only be occupied by a single player at any given time.
4. Players can only move left, right, up or down.
3. Each player's starting location & movement direction are chosen randomly at the beginning of the game.
5. If the player is going to move off the grid, their direction is reversed by the referee so they begin moving back the way they came.

Instructions
=================================================================================

The project is based on Spring Boot v1.3.3.
Dependencies to rebuild: Maven, Java 1.8.
Dependencies to run: Java 1.8

1: To run the application it must be packaged into a jar using the maven goal "maven package".

This will generate an executable jar file in the projectdir/target directory and will be titled: "kristians-console-game-1.0-SNAPSHOT.jar"

2: To run this jar, open a terminal and execute the command:
 "java -jar kristians-console-game-1.0-SNAPSHOT.jar"

 The game is text only and will outputted directly into the console.
 The game ends when 9 players have been permanently banned from play and only one player remains.

 Due to the random chance involved in this game and lengthy cool down time when a player is sent off, when it comes down to just 2 players remaining, it can take a long time to complete.

 To accelerate the game, the static properties declared in the GameBoard class: MOVEMENT_TICK & PLAYER_FOUL_DURATION can be reduced.

 You must then re-run step 1 and 2 if this change is made.
